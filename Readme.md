# Clean commits

Clean commits es una actualización del código desarrollado durante el curso *Clean Commits en proyectos NPM* y que imparte [Joseba Fuentes](https://github.com/Tlaloc-Es). Se trata de una configuración de inicio que permite mantener limpios los commits.

Mediante git-hooks se aplica un lint al código, se testea (JS) y se verifica que el mensaje de commit sigue 'conventional commits'. Además al hacer release, se genera un archivo CHANGELOG según los mensajes de los commits.

Los git-hooks se gestionan con [Husky](https://typicode.github.io/husky/#/):
- pre-commit:
  - lint con [prettier](https://prettier.io/)
  - test con [jest](https://jestjs.io/)
- commit-msg": [commitlint](https://commitlint.js.org) siguiendo [conventional commits](https://www.conventionalcommits.org/es/)

`npm install`

`npm run release -- --first-release` para generar la primera versión del proyecto.
`npm run release` para generar/actualizar automáticamente el archivo CHANGELOG e incrementar la versión de npm.
`npm run release -- --release-as major` para generar/actualizar automáticamente el archivo CHANGELOG e incrementar la versión de npm especificando con el argumento si es `major`, `patch` o `minor`.